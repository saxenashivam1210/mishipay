package main

import (
	"github.com/jasonlvhit/gocron"
	service "mishipay"
	h "mishipay/handler"
)



func main() {

	// init all dbs, constants, engines
	service.Init()


	//to let refresh the inventory from shopify every 10 seconds
	go func() {
		gocron.Every(5).Second().Do(service.RefreshInventory)
		<-gocron.Start()
	}()

	// http server for handling requests
	h.HandleRequests()


}
