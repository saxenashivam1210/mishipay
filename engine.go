package mishipay

import (

	"github.com/go-redis/redis/v7"
	"log"
	"mishipay/model"
	"mishipay/provider"

)

var ServiceEngine *Engine

type ShopifyClient struct{
	BaseUrl string
}

type Engine struct {
	Redis  *redis.Client
	Shopify ShopifyClient
}

func (c *ShopifyClient) GetBaseUrl() string{
	return c.BaseUrl
}
func (e *Engine) GetRedisClient() *redis.Client{
	return e.Redis
}

func Init(){

	// to be picked from OS env later
	Redis := initRedisClient("localhost:6379")
	shopifyBaseUrl := "https://a38f4a6a8cb713fe2bebdbf3df331f54:3182dcd29ff6c3f6f2dd325ba99b4216@mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2021-01"
	ServiceEngine = &Engine{Redis:Redis, Shopify:ShopifyClient{BaseUrl:shopifyBaseUrl}}
}


func initRedisClient(addr string) *redis.Client {
	opts := &redis.Options{
		Addr: addr,
	}
	client := redis.NewClient(opts)
	res, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
	if res != "PONG" {
		panic("Didn't receive <PONG>")
	}
	return client
}



func HealthCheck(req *model.RequestHealth) *model.ResponseHealth {
	if req.Input == "PING"{
		return &model.ResponseHealth{Output:"PONG"}
	}
	return nil
}


func RefreshInventory(){
	log.Println("RefreshInventory: refreshing")
	inventory, err := provider.GetInventory(ServiceEngine.Shopify.GetBaseUrl())
	if err != nil{
		log.Fatal("RefreshInventory: failed to refresh inventory", err)
		panic(err)
	}
	err = ServiceEngine.IngestInventory(inventory)
	if err != nil{
		log.Fatal("RefreshInventory: failed to ingest inventory", err)
		panic(err)
	}
}


func GetInventory() (*model.ShopifyInventory, error){
	log.Println("GetInventory: getting inventory")
	inventory, err := ServiceEngine.GetInventory(provider.SHOPIFY)
	if err != nil{
		log.Fatal("GetInventory: failed", err)
		return nil, err
	}
	return inventory, err
}

func CreateOrder(request *model.OrderRequest) (*model.ShopifyOrderResponse, error){
	log.Println("CreateOrder: creating order", request.VariantId, request.Quantity)
	variantId := request.VariantId
	quantity := request.Quantity
	lineItem :=  model.LineItem{
		VariantID: variantId,
		Quantity:  quantity,
	}
	lineItems := make([]model.LineItem, 0)
	lineItems = append(lineItems, lineItem)
	orderRequest := &model.ShopifyOrderRequest{Order:model.Order{LineItems:lineItems}}
	order, err := provider.CreateOrder(orderRequest, ServiceEngine.Shopify.GetBaseUrl())
	if err != nil{
		log.Fatal("CreateOrder: failed", err, request.VariantId, request.Quantity)
		return nil, err
	}
	return order, nil
}