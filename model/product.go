package model

type GetProductListResponse struct{
	ProductList []Product
}

type OrderRequest struct {
	VariantId int64
	Quantity int
}

type Product struct{

}