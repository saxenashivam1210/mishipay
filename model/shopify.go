package model

type ShopifyInventory struct {
	ShopifyProducts []struct {
		ID                int64       `json:"id"`
		Title             string      `json:"title"`
		BodyHTML          interface{} `json:"body_html"`
		Vendor            string      `json:"vendor"`
		ProductType       string      `json:"product_type"`
		CreatedAt         string      `json:"created_at"`
		Handle            string      `json:"handle"`
		UpdatedAt         string      `json:"updated_at"`
		PublishedAt       string      `json:"published_at"`
		TemplateSuffix    interface{} `json:"template_suffix"`
		Status            string      `json:"status"`
		PublishedScope    string      `json:"published_scope"`
		Tags              string      `json:"tags"`
		AdminGraphqlAPIID string      `json:"admin_graphql_api_id"`
		Variants          []struct {
			ID                   int64       `json:"id"`
			ProductID            int64       `json:"product_id"`
			Title                string      `json:"title"`
			Price                string      `json:"price"`
			Sku                  string      `json:"sku"`
			Position             int         `json:"position"`
			InventoryPolicy      string      `json:"inventory_policy"`
			CompareAtPrice       interface{} `json:"compare_at_price"`
			FulfillmentService   string      `json:"fulfillment_service"`
			InventoryManagement  interface{} `json:"inventory_management"`
			Option1              string      `json:"option1"`
			Option2              interface{} `json:"option2"`
			Option3              interface{} `json:"option3"`
			CreatedAt            string      `json:"created_at"`
			UpdatedAt            string      `json:"updated_at"`
			Taxable              bool        `json:"taxable"`
			Barcode              interface{} `json:"barcode"`
			Grams                int         `json:"grams"`
			ImageID              interface{} `json:"image_id"`
			Weight               float64     `json:"weight"`
			WeightUnit           string      `json:"weight_unit"`
			InventoryItemID      int64       `json:"inventory_item_id"`
			InventoryQuantity    int         `json:"inventory_quantity"`
			OldInventoryQuantity int         `json:"old_inventory_quantity"`
			RequiresShipping     bool        `json:"requires_shipping"`
			AdminGraphqlAPIID    string      `json:"admin_graphql_api_id"`
		} `json:"variants"`
		Options []struct {
			ID        int64    `json:"id"`
			ProductID int64    `json:"product_id"`
			Name      string   `json:"name"`
			Position  int      `json:"position"`
			Values    []string `json:"values"`
		} `json:"options"`
		Images []interface{} `json:"images"`
		Image  interface{}   `json:"image"`
	} `json:"products"`
}


type ShopifyOrderRequest struct {
	Order Order `json:"order"`
}
type Order struct {
	LineItems []LineItem `json:"line_items"`
}
type LineItem struct{
	VariantID int64 `json:"variant_id"`
	Quantity  int   `json:"quantity"`
}

type ShopifyOrderResponse struct {
	Order ShopifyOrder  `json:"order"`
}
type ShopifyOrder struct {
	Id int64 `json:"id"`
}