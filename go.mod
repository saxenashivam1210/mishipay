module mishipay

go 1.14

require (
	github.com/go-redis/redis/v7 v7.4.0
	github.com/jasonlvhit/gocron v0.0.1 // indirect
)
