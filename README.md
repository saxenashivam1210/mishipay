

# Mishipay

Steps to setup dependencies: go mod download
Steps to run: go run main/run.go 

Note: redis is required as primary database for application to start

## Get List of Products: curl --location --request GET 'http://localhost:8000/v1/inventory'





## Create Order: curl --location --request POST 'http://localhost:8000/v1/order' --header 'Content-Type: application/json' --data-raw '{"variantId": 31436805668931,"quantity": 1}'
### Response: {"order": {"id": 3708335849539}}
