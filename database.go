package mishipay

import (
	"encoding/json"
	"log"
	"mishipay/model"
	"mishipay/provider"
	"time"
)

func(e *Engine) IngestInventory(inventory *model.ShopifyInventory) error{
	redisClient := e.GetRedisClient()
	cacheDuration, err := time.ParseDuration("48h")
	if err != nil {
		log.Fatal("Database: unable to parse cache duration during ingest")
		return Internal()
	}
	data, err := json.Marshal(inventory.ShopifyProducts)
	if err != nil {
		log.Fatal("Database: unable to parse json during ingest")
		return Internal()
	}
	err = redisClient.Set(provider.SHOPIFY, data, cacheDuration).Err()
	if err != nil {
		log.Fatal("Database: unable to set json in cache during ingest")
		return Internal()
	}
	return nil
}

func(e *Engine) GetInventory(shopProvider string) (*model.ShopifyInventory, error){
	redisClient := e.GetRedisClient()
	cachedResult, err := redisClient.Get(shopProvider).Result()
	if err != nil {
		return nil, err
	}
	cachedResponse := &model.ShopifyInventory{}
	err = json.Unmarshal([]byte(cachedResult), &cachedResponse.ShopifyProducts)
	if err != nil {
		log.Fatal("Database: unable to get data from cache", err)
		return nil, err
	}
	return cachedResponse, nil
}
