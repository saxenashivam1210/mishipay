package mishipay

import (
	"context"
	"errors"
	"fmt"
)

var (
	contextCancelled = Error{Code: 513, Name: "internal", Description: context.Canceled.Error()}
)

func Internal() Error {
	return Error{Code: 500, Name: "internal"}
}
type Error struct {
	Code        int    `json:"code"`
	Name        string `json:"error"`
	Description string `json:"-"`
}

func (e Error) Error() string {
	if e.Description == "" {
		return e.Name
	} else {
		return fmt.Sprintf("%s (%s)", e.Name, e.Description)
	}
}

func FromError(err error) Error {
	var e Error
	if errors.As(err, &e) {
		return e
	} else {
		if errors.Is(err, context.Canceled) {
			return contextCancelled
		}

		e = Internal()
		e.Description = err.Error()
		return e
	}
}