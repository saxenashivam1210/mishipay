package handler

import (
	"encoding/json"
	"log"
	"mishipay"
	"mishipay/model"
	"net/http"
)


func HandleRequests() {

	http.HandleFunc("/v1/health", health)
	http.HandleFunc("/v1/inventory", getInventory)
	http.HandleFunc("/v1/order", createOrder)

	log.Println("Starting server")

	log.Fatal(http.ListenAndServe(":8000", nil))
}


func health(w http.ResponseWriter, r *http.Request){
	req := new(model.RequestHealth)
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Fatal("health: failed to read request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	res := mishipay.HealthCheck(req)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(res)
}

func getInventory(w http.ResponseWriter, r *http.Request){
	res, err := mishipay.GetInventory()
	if err != nil {
		e := mishipay.FromError(err)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(e.Code)
		json.NewEncoder(w).Encode(e)
	}else{
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(res)
	}

}


func createOrder(w http.ResponseWriter, r *http.Request){
	if r.Method == "POST"{
		req := new(model.OrderRequest)
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			log.Fatal("health: failed to read request")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		res, err := mishipay.CreateOrder(req)
		if err != nil {
			e := mishipay.FromError(err)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(e.Code)
			json.NewEncoder(w).Encode(e)
		}else{
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(res)
		}
	}else{
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(405)
	}


}