package provider

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"mishipay/model"
	"net/http"
	"strings"
)

const SHOPIFY  = "SHOPIFY"


func GetInventory(baseUrl string ) (*model.ShopifyInventory, error){
	url := baseUrl+"/products.json"
	method := "GET"
	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		log.Fatal("getInventory: failed to create request", err)
		return nil, err
	}

	res, err := client.Do(req)
	if err != nil {
		log.Fatal("getInventory: failed api call", err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal("getInventory: failed to read response", err)
		return nil, err
	}

	//fmt.Println(string(body))
	inventory := &model.ShopifyInventory{}
	err = json.Unmarshal(body, inventory)

	if err != nil {
		log.Fatal("getInventory: failed to convert response to model", err)
		return nil, err
	}

	return inventory, nil
}

func CreateOrder(request *model.ShopifyOrderRequest, baseUrl string) (*model.ShopifyOrderResponse, error){
	url := baseUrl+"/orders.json"
	method := "POST"

	req1, err := json.Marshal(request)
	if err != nil{
		log.Fatal("CreateOrder: failed to parse request", err)
		return nil, err
	}
	payload := strings.NewReader(string(req1))

	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		log.Fatal("CreateOrder: failed to create request", err)
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		log.Fatal("CreateOrder: failed to create api call", err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal("CreateOrder: failed to read response", err)
		return nil, err
	}

	log.Println(url)
	log.Println(request.Order.LineItems)
	log.Println(string(body))


	order := &model.ShopifyOrderResponse{}
	err = json.Unmarshal(body, order)

	if err != nil {
		log.Fatal("CreateOrder: failed to convert response to model", err)
		return nil, err
	}

	return order, nil
}

